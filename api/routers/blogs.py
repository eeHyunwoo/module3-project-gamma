from fastapi import APIRouter, Response, Depends
from queries.blog import BlogQueries

router = APIRouter()

@router.get("/api/blogs")
def get_all_blogs(
    response: Response,
    blog: BlogQueries = Depends(),
):
    try:
        return blog.get_all()
    except:
        response.status_code = 400
        return {"message": "Could not retrieve blogs."}
