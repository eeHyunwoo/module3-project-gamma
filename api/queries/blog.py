from pydantic import BaseModel
from typing import Optional, List, Union
from datetime import datetime
from .pool import pool

class Error(BaseModel):
    message:str


class BlogOut(BaseModel):
    id: int
    title: str
    body: str
    image: Optional[str]
    created_on: datetime=datetime.now()
    author_id: int

class BlogIn(BaseModel):
    title: str
    body: str
    image: Optional[str]

class BlogQueries:
    def get_all(self) -> Union[List[BlogOut], Error]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                    Select id, title, body, image, created_on, author_id
                    FROM blog
                    ORDER BY created_on DESC
                    """
                )
                return [self.record_to_blog_out(record) for record in result]

    def record_to_blog_out(self, record):
        return BlogOut(
            id=record[0],
            title=record[1],
            body=record[2],
            image=record[3],
            created_on=record[4],
            author_id=record[5],
        )
