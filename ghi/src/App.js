import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Signup from './Signup.js';
import LoginForm from './LoginForm.js';
import Construct from "./Construct.js";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Catalog from './Catalog.js';

function App() {
  return (
    <>
      <Routes>
        <Route path="/home" element={<Construct />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/login" element={<LoginForm />} />
        <Route path="/catalog" element={<Catalog />} />
      </Routes>
      <ToastContainer />
    </>
  );
}

export default App;
