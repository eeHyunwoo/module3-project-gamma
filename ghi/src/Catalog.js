import React from "react";
import "./Construct.css";
import "./Catalog.css";

function Catalog(props) {

    return (
    <div className="App">
      <header className="App-header">
        <div className="top-bar">
          <ul>
            <li><a href="/home">Home Page</a></li>
            <li><a href="/">Blog</a></li>
            <li><a href="/">Competition</a></li>
            <li><a href="/catalog">Catalog/ Plant shop</a></li>
            <li><a href="/">Plant Care</a></li>
          </ul>
        </div>
      </header>
      <div>
            <h1>
                Let's add something to your garden! 
            </h1>
            <h2></h2>
      </div>
      <div id="body" className="grid-container">
        <div className="grid-item1">
            <a href="https://example.com">
                Green House This Way
            <img id="greenhouse" src="greenhouse.png" alt="Image 1" />
            </a>
        </div>
        <div className="grid-item2">
            <a href="https://example.com">
                Supplies This Way
            <img id="supply" src="gardensupply.png" alt="Image 2" />
            </a>
        </div>
    </div>
    </div>
  );
}

export default Catalog;
