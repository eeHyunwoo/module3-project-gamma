import { configureStore } from "@reduxjs/toolkit";
import { authApi } from "./authAPI";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import { userSlice } from "./user";


export const store = configureStore({
    reducer: {
        auth: userSlice.reducer,
        [authApi.reducerPath]: authApi.reducer,
    },
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware()
            .concat(authApi.middleware),
});

setupListeners(store.dispatch);
